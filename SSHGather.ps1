$computers = Get-ADComputer -filter {name -like "*"}
function Test-PortAlive {
    ######################################
    ##Function:         Test-PortAlive   #
    ##                                   #
    ##Created by:       Noam Wajnman     #
    ######################################
    [CmdletBinding()]
    [OutputType([System.boolean])]
    param(
        [Parameter(ValueFromPipeline=$true)][System.String[]]$server,
        [int]$port
    )
    $socket = new-object Net.Sockets.TcpClient
    $connect = $socket.BeginConnect($server, $port, $null, $null)
    $NoTimeOut = $connect.AsyncWaitHandle.WaitOne(200, $false)
    if ($NoTimeOut) {
        $socket.EndConnect($connect) | Out-Null
        return $true               
    }
    else {
        return $false
    }
}

foreach ($pc in $computers){
if((Test-PortAlive -port 5985 $pc.Name)){
Write-Host $pc.name
    #$userspc = Invoke-Command  -ComputerName $pc.name {Get-WmiObject win32_useraccount -ErrorAction Stop} | select domain,name,sid
    $userspc = Invoke-Command  -ComputerName $pc.name  { New-PSDrive -Name HKU -PSProvider Registry -Root Registry::HKEY_USERS | Out-Null; (Get-ChildItem HKU: | where { $_.Name -match 'S-\d-\d+-(\d+-){1,14}\d+$' }).PSChildName}
    
    if ($userspc.Count -gt 0){
        foreach ($usersid in $userspc){
            
                $username = Invoke-Command  -ComputerName $pc.name {Get-WmiObject win32_useraccount -Filter "sid='$($using:usersid)'"} | Select-Object name
                if(!$username){
                    $username = Get-WmiObject win32_useraccount -Filter "sid='$($usersid)'" | Select-Object name
                }
                # SecureCRT
                $dataCRT = Invoke-Command  -ComputerName $pc.name {New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS;Get-ItemProperty "HKU:\$($using:usersid)\Software\VanDyke\SecureCRT" -ErrorAction SilentlyContinue}  | Select-Object "Config Path"
                if ($dataCRT.Count -gt 0){
                    Write-Host "$($pc.name) - $($username.name) - $($usersid) #SecureCRT"
                    Write-Host $dataCRT[1] -ForegroundColor Yellow -BackgroundColor Black
                    $dirs = Invoke-Command -ComputerName $pc.name {dir "$($using:dataCRT[1].'Config Path')\Sessions"}| Select-Object name
                    Write-Host "Dirs on: $($dataCRT[1].'Config Path')\Sessions" -ForegroundColor Green -BackgroundColor Black
                    $dirs | ForEach-Object {Write-Host $_.name -ForegroundColor Yellow -BackgroundColor Black}
                    # WORK ON EXTRACT DATA FROM SESS
                }
                # WinSCP
                $dataSCP = Invoke-Command -ComputerName $pc.name {New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS;Get-ChildItem "HKU:\$($using:usersid)\Software\Martin Prikryl\WinSCP 2\Sessions" -ErrorAction SilentlyContinue}
                if ($dataSCP.Count -gt 0){
                    Write-Host "$($pc.name) - $($username.name) - $($usersid) #WinSCP"
                    #foreach
                    $dataSCP | ForEach-Object {Write-Host $_ -ForegroundColor Yellow -BackgroundColor Black}
                    # WORK ON EXTRACT DATA FROM SESS
                }

                # SSH Secure Shell
                $dataSecure = Invoke-Command  -ComputerName $pc.name {New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS;Get-ItemProperty "HKU:\$($using:usersid)\Software\SSH Communications Security\SSH Secure Shell" -ErrorAction SilentlyContinue}  | Select-Object "UserKeysDir"
                if ($dataSecure.Count -gt 0){
                    Write-Host "$($pc.name) - $($username.name) - $($usersid) #SSH Secure Shell"
                    Write-Host $dataSecure[1] -ForegroundColor Yellow -BackgroundColor Black
                    $dirs = Invoke-Command -ComputerName $pc.name {dir "$($using:dataSecure[1].'UserKeysDir')"}| Select-Object name
                    Write-Host "Dirs on: $($dataSecure[1].'UserKeysDir')" -ForegroundColor Green -BackgroundColor Black
                    $dirs | ForEach-Object {Write-Host $_.name -ForegroundColor Yellow -BackgroundColor Black}
                    # WORK ON EXTRACT DATA FROM SESS
                }

                # Putty
                $dataPutty = Invoke-Command -ComputerName $pc.name {New-PSDrive -PSProvider Registry -Name HKU -Root HKEY_USERS;Get-ChildItem "HKU:\$($using:usersid)\Software\SimonTatham\Putty\Sessions" -ErrorAction SilentlyContinue}
                if ($dataPutty.Count -gt 0){
                    Write-Host "$($pc.name) - $($username.name) - $($usersid) #Putty"
                    #foreach
                    $dataPutty | ForEach-Object {Write-Host $_ -ForegroundColor Yellow -BackgroundColor Black}
                    # WORK ON EXTRACT DATA FROM SESS
                }
            #Write-Host $pc
            }
        }
    }
}